package com.wemirr.platform.authority.service;

import com.wemirr.framework.db.mybatisplus.ext.SuperService;
import com.wemirr.platform.authority.domain.common.entity.SiteMessage;

/**
 * @author Levin
 */
public interface SiteMessageService extends SuperService<SiteMessage> {


}
