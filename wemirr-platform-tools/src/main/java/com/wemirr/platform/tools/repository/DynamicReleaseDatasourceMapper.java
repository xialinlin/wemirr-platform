package com.wemirr.platform.tools.repository;

import com.wemirr.framework.db.mybatisplus.ext.SuperMapper;
import com.wemirr.platform.tools.domain.entity.DynamicReleaseDatasource;
import org.springframework.stereotype.Repository;

/**
 * @author Levin
 */
@Repository
public interface DynamicReleaseDatasourceMapper extends SuperMapper<DynamicReleaseDatasource> {



}
